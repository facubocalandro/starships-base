package edu.austral.dissis.starship.controller;

import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.utils.Pair;

import java.util.Optional;

public interface InputHandler<T extends GameObject> {
    Pair<Optional<T>, InputHandler<T>> handle(Key key);
}
