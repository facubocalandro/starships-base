package edu.austral.dissis.starship.controller;

import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.view.ObjectUpdater;

public interface Updatable {
    GameObject update(ObjectUpdater objectUpdater);
}
