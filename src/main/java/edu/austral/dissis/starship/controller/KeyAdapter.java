package edu.austral.dissis.starship.controller;

import java.util.Map;
import java.util.Optional;

public class KeyAdapter{
    private final Map<Integer, Key> keyMap;

    public KeyAdapter(Map<Integer, Key> keyMap) {
        this.keyMap = keyMap;
    }

    public Optional<Key> adapt(Integer key) {
        return Optional.ofNullable(keyMap.get(key));
    }
}
