package edu.austral.dissis.starship.controller;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.model.Movable;
import edu.austral.dissis.starship.utils.Pair;

import java.util.Optional;

import static edu.austral.dissis.starship.utils.Utils.*;

public class MovementHandler<T extends Movable<T> & GameObject> implements InputHandler<T>{
    T movable;

    public MovementHandler(T movable) {
        this.movable = movable;
    }

    @Override
    public Pair<Optional<T>, InputHandler<T>> handle(Key key) {
            switch (key) {
                case MOVE_FORWARD:
                    T newMovableForward = movable;
                    if (movable.canMoveForward(Vector2.vector(WINDOW_WIDTH, WINDOW_HEIGHT), SPACESHIP_SPEED)){
                        newMovableForward = movable.moveForward(SPACESHIP_SPEED);
                    }
                    return Pair.pair(Optional.ofNullable(newMovableForward), new MovementHandler<>(newMovableForward));
                case MOVE_BACKWARDS:
                    T newMovableBackwards = movable;
                    if (movable.canMoveBackwards(Vector2.vector(WINDOW_WIDTH, WINDOW_HEIGHT), SPACESHIP_SPEED)){
                        newMovableBackwards = movable.moveBackwards(SPACESHIP_SPEED);
                    }
                    return Pair.pair(Optional.ofNullable(newMovableBackwards), new MovementHandler<>(newMovableBackwards));
                case ROTATE_LEFT:
                    T newMovableLeft = movable.rotate(-ROTATION);
                    return Pair.pair(Optional.ofNullable(newMovableLeft), new MovementHandler<>(newMovableLeft));
                case ROTATE_RIGHT:
                    T newMovableRight = movable.rotate(ROTATION);
                    return Pair.pair(Optional.ofNullable(newMovableRight), new MovementHandler<>(newMovableRight));
                default:
                    return Pair.pair(Optional.ofNullable(movable), this);
            }
    }
}
