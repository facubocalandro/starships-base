package edu.austral.dissis.starship.controller;

public enum Key {
    ROTATE_RIGHT,
    ROTATE_LEFT,
    MOVE_FORWARD,
    MOVE_BACKWARDS,
    SHOOT
}
