package edu.austral.dissis.starship.controller;

import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.model.Shootable;
import edu.austral.dissis.starship.utils.Pair;

import java.util.Optional;

public class ShootHandler<T extends Shootable & GameObject> implements InputHandler<Bullet> {
    private final T shootable;
    public ShootHandler(T shootable){
        this.shootable = shootable;
    }

    @Override
    public Pair<Optional<Bullet>, InputHandler<Bullet>> handle(Key key) {

        if (key == Key.SHOOT) {
            return Pair.pair(shootable.shoot(), this);
        }
        return Pair.pair(Optional.empty(), this);
    }
}
