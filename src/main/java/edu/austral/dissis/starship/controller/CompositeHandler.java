package edu.austral.dissis.starship.controller;

import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.model.Movable;
import edu.austral.dissis.starship.model.Shootable;
import edu.austral.dissis.starship.utils.Pair;

import java.util.Optional;

public class CompositeHandler<A extends Shootable & GameObject & Movable<A>> implements InputHandler<GameObject>{
    private final ShootHandler<A> shootHandler;
    private final MovementHandler<A> movementHandler;

    public CompositeHandler(ShootHandler<A> shootHandler, MovementHandler<A> movementHandler) {
        this.shootHandler = shootHandler;
        this.movementHandler = movementHandler;
    }

    @Override
    public Pair<Optional<GameObject>, InputHandler<GameObject>> handle(Key key) {

        Pair<Optional<Bullet>, InputHandler<Bullet>> shootPair = shootHandler.handle(key);
        Bullet bullet = shootPair.first.orElse(null);
        if (bullet != null){
            return Pair.pair(Optional.of(bullet), this);
        }

        Pair<Optional<A>, InputHandler<A>> movePair = movementHandler.handle(key);
        A gameObject = movePair.first.orElse(null);
        if (gameObject != null){
            return Pair.pair(Optional.of(gameObject), new CompositeHandler<>(new ShootHandler<>(gameObject), new MovementHandler<>(gameObject)));
        }

        return Pair.pair(Optional.empty(), this);
    }
}
