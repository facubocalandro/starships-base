package edu.austral.dissis.starship.utils;

public enum GameMode {
    SINGLE_PLAYER,
    MULTI_PLAYER
}
