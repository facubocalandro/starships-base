package edu.austral.dissis.starship.utils;

import edu.austral.dissis.starship.base.framework.ImageLoader;
import edu.austral.dissis.starship.controller.Key;
import edu.austral.dissis.starship.view.Locatable;
import processing.core.PConstants;
import processing.core.PImage;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    public static long currentId = 0;
    public static int SPACESHIP_WIDTH = 80;
    public static int SPACESHIP_HEIGHT = 80;
    public static int BIG_ASTEROID_WIDTH = 90;
    public static int BIG_ASTEROID_HEIGHT = 90;
    public static int MEDIUM_ASTEROID_WIDTH = 60;
    public static int MEDIUM_ASTEROID_HEIGHT = 60;
    public static int SMALL_ASTEROID_WIDTH = 30;
    public static int SMALL_ASTEROID_HEIGHT = 30;
    public static int BULLET_WIDTH = 20;
    public static int BULLET_HEIGHT = 30;
    public static int ASTEROID_INTERVAL = 1000;
    public static int SHOOT_INTERVAL = 500;
    public static int ASTEROID_SPEED = 2;
    public static float ROTATION = PConstants.PI / 60;
    public static int SPACESHIP_SPEED = 2;
    public static int WINDOW_HEIGHT = 1000;
    public static int WINDOW_WIDTH = 1000;
    public static int HIT_SPACESHIP_POINTS = 10;
    public static int INITIAL_LIVES = 5;
    public static int INITIAL_BULLETS = 100;

    public static float calculateRotation(Locatable locatable) {
        return locatable.getDirection().rotate(PConstants.PI / 2).getAngle();
    }

    public static PImage getSpaceshipImage(ImageLoader imageLoader){
        PImage spaceshipImage = imageLoader.load("src/main/resources/spaceship.png");
        spaceshipImage.resize(SPACESHIP_WIDTH, SPACESHIP_HEIGHT);

        return spaceshipImage;
    }

    public static PImage getBigAsteroidImage(ImageLoader imageLoader){
        PImage asteroidImage = imageLoader.load("src/main/resources/asteroid.png");
        asteroidImage.resize(BIG_ASTEROID_WIDTH, BIG_ASTEROID_HEIGHT);

        return asteroidImage;
    }

    public static PImage getMediumAsteroidImage(ImageLoader imageLoader){
        PImage asteroidImage = imageLoader.load("src/main/resources/asteroid.png");
        asteroidImage.resize(MEDIUM_ASTEROID_WIDTH, MEDIUM_ASTEROID_HEIGHT);

        return asteroidImage;
    }

    public static PImage getSmallAsteroidImage(ImageLoader imageLoader){
        PImage asteroidImage = imageLoader.load("src/main/resources/asteroid.png");
        asteroidImage.resize(SMALL_ASTEROID_WIDTH, SMALL_ASTEROID_HEIGHT);

        return asteroidImage;
    }

    public static PImage getBulletImage(ImageLoader imageLoader){
        PImage bulletImage = imageLoader.load("src/main/resources/bullet.png");
        bulletImage.resize(BULLET_WIDTH, BULLET_HEIGHT);

        return bulletImage;
    }

    public static Map<Integer, Key> getKeys(int i){
        Map<Integer, Key> keys = new HashMap<>();
        switch (i){
            case 0:
                keys.put(KeyEvent.VK_RIGHT, Key.ROTATE_RIGHT);
                keys.put(KeyEvent.VK_LEFT, Key.ROTATE_LEFT);
                keys.put(KeyEvent.VK_DOWN, Key.MOVE_BACKWARDS);
                keys.put(KeyEvent.VK_UP, Key.MOVE_FORWARD);
                keys.put(KeyEvent.VK_SPACE, Key.SHOOT);
                break;
            case 1:
                keys.put(KeyEvent.VK_D, Key.ROTATE_RIGHT);
                keys.put(KeyEvent.VK_A, Key.ROTATE_LEFT);
                keys.put(KeyEvent.VK_S, Key.MOVE_BACKWARDS);
                keys.put(KeyEvent.VK_W, Key.MOVE_FORWARD);
                keys.put(KeyEvent.VK_R, Key.SHOOT);
        }
        return keys;
    }
}
