package edu.austral.dissis.starship.utils;

import edu.austral.dissis.starship.model.Spaceship;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.austral.dissis.starship.utils.Utils.INITIAL_LIVES;

public class Stats {
    private final Map<Long, Integer> spaceshipPoints;
    private final Map<Long, Integer> spaceshipLives;


    public Stats(List<Spaceship> spaceships) {
        spaceshipPoints = new HashMap<>();
        spaceshipLives = new HashMap<>();
        spaceships.forEach(spaceship -> {
            spaceshipPoints.put(spaceship.getId(), 0);
            spaceshipLives.put(spaceship.getId(), INITIAL_LIVES);
        });
    }

    public Integer getSpaceshipLives(Long spaceshipId){
        return spaceshipLives.get(spaceshipId);
    }

    public Integer getSpaceshipPoints(Long spaceshipId){
        return spaceshipPoints.get(spaceshipId);
    }

    public void reduceLives(Long spaceshipId, Integer lives){
        spaceshipLives.replace(spaceshipId, spaceshipLives.get(spaceshipId) - lives);
    }

    public void increasePoints(Long spaceshipId, Integer points){
        spaceshipPoints.replace(spaceshipId, spaceshipPoints.get(spaceshipId) + points);
    }

    public Map<Long, Integer> getSpaceshipLives() {
        return spaceshipLives;
    }
}
