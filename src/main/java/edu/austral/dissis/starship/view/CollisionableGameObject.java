package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.base.collision.Collisionable;
import edu.austral.dissis.starship.model.Asteroid;
import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.Spaceship;

public interface CollisionableGameObject extends Collisionable<CollisionableGameObject> {
    default void collisionedWithAsteroid(Asteroid asteroid){}
    default void collisionedWithSpaceship(Spaceship spaceship){}
    default void collisionedWithBullet(Bullet bullet){}
}
