package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.ObjectsContainer;
import edu.austral.dissis.starship.view.CollisionableGameObject;

public interface AsCollisionable {
    CollisionableGameObject asCollisionable(ObjectsContainer objectsContainer);
}
