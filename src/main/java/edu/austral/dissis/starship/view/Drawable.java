package edu.austral.dissis.starship.view;

import processing.core.PGraphics;

public interface Drawable {
    void draw(PGraphics graphics, GameObjectDrawer drawer);
}
