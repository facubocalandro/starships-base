package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.ObjectsContainer;
import edu.austral.dissis.starship.model.Spaceship;

import static edu.austral.dissis.starship.utils.Utils.*;

public class CollisionableSpaceship extends CollisionableImpl<Spaceship>{
    public CollisionableSpaceship(Spaceship spaceship, ObjectsContainer objectsContainer) {
        super(spaceship, objectsContainer, SPACESHIP_WIDTH, SPACESHIP_HEIGHT);
    }

    @Override
    public void collisionedWith(CollisionableGameObject collisionable) {
        collisionable.collisionedWithSpaceship(object);
    }

    @Override
    public void collisionedWithBullet(Bullet bullet) {
        if (object.getId() != bullet.getShooterId()){
            objectsContainer.increasePoints(bullet.getShooterId(), HIT_SPACESHIP_POINTS);
            objectsContainer.reduceLives(object.getId(), 1);
            objectsContainer.removeGameObject(bullet);
        }
    }
}
