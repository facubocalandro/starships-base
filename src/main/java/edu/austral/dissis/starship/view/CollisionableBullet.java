package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.Asteroid;
import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.ObjectsContainer;

import static edu.austral.dissis.starship.utils.Utils.*;

public class CollisionableBullet extends CollisionableImpl<Bullet> {

    public CollisionableBullet(Bullet bullet, ObjectsContainer objectsContainer) {
        super(bullet, objectsContainer, BULLET_WIDTH, BULLET_HEIGHT);
    }

    @Override
    public void collisionedWith(CollisionableGameObject collisionable) {
        collisionable.collisionedWithBullet(object);
    }

    @Override
    public void collisionedWithAsteroid(Asteroid asteroid) {
        objectsContainer.removeGameObject(object);
    }
}
