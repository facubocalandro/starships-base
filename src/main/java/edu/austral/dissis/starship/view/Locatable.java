package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.base.vector.Vector2;

public interface Locatable {
    Vector2 getPosition();
    Vector2 getDirection();
}
