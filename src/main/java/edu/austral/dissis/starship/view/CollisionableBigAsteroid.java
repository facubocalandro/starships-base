package edu.austral.dissis.starship.view;
import edu.austral.dissis.starship.model.*;

import static edu.austral.dissis.starship.utils.Utils.*;

public class CollisionableBigAsteroid extends CollisionableImpl<BigAsteroid> {

    public CollisionableBigAsteroid(BigAsteroid asteroid, ObjectsContainer objectsContainer){
        super(asteroid, objectsContainer, BIG_ASTEROID_WIDTH, BIG_ASTEROID_HEIGHT);
    }

    @Override
    public void collisionedWith(CollisionableGameObject collisionable) {
        collisionable.collisionedWithAsteroid(object);
    }

    @Override
    public void collisionedWithBullet(Bullet bullet) {
        objectsContainer.removeGameObject(object);
    }

    @Override
    public void collisionedWithSpaceship(Spaceship spaceship) { objectsContainer.removeGameObject(object); }
}
