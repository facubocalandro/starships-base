package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.GameObject;
import edu.austral.dissis.starship.model.ObjectsContainer;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import static edu.austral.dissis.starship.utils.Utils.*;

public abstract class CollisionableImpl<T extends GameObject> implements CollisionableGameObject{
    T object;
    ObjectsContainer objectsContainer;
    int width;
    int height;

    public CollisionableImpl(T object, ObjectsContainer objectsContainer, int width, int height) {
        this.object = object;
        this.objectsContainer = objectsContainer;
        this.width = width;
        this.height = height;
    }

    @Override
    public Shape getShape() {
        final Rectangle2D baseSquare = new Rectangle2D.Float(width/ -2,height / -2,width, height);
        final Path2D.Float path = new Path2D.Float();
        path.append(baseSquare, false);

        final AffineTransform transform = new AffineTransform();
        transform.translate(object.getPosition().getX(), object.getPosition().getY());
        transform.rotate(calculateRotation(object));

        path.transform(transform);

        return path;
    }

}
