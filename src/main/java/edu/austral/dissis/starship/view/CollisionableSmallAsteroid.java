package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.ObjectsContainer;
import edu.austral.dissis.starship.model.SmallAsteroid;
import edu.austral.dissis.starship.model.Spaceship;

import static edu.austral.dissis.starship.utils.Utils.SMALL_ASTEROID_HEIGHT;
import static edu.austral.dissis.starship.utils.Utils.SMALL_ASTEROID_WIDTH;

public class CollisionableSmallAsteroid extends CollisionableImpl<SmallAsteroid> {
    public CollisionableSmallAsteroid(SmallAsteroid object, ObjectsContainer objectsContainer) {
        super(object, objectsContainer, SMALL_ASTEROID_WIDTH, SMALL_ASTEROID_HEIGHT);
    }

    @Override
    public void collisionedWith(CollisionableGameObject collisionable) {
        collisionable.collisionedWithAsteroid(object);
    }

    @Override
    public void collisionedWithBullet(Bullet bullet) {
        objectsContainer.removeGameObject(object);
    }

    @Override
    public void collisionedWithSpaceship(Spaceship spaceship) { objectsContainer.removeGameObject(object); }
}
