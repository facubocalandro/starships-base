package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.model.Bullet;
import edu.austral.dissis.starship.model.MediumAsteroid;
import edu.austral.dissis.starship.model.ObjectsContainer;
import edu.austral.dissis.starship.model.Spaceship;

import static edu.austral.dissis.starship.utils.Utils.MEDIUM_ASTEROID_HEIGHT;
import static edu.austral.dissis.starship.utils.Utils.MEDIUM_ASTEROID_WIDTH;

public class CollisionableMediumAsteroid extends CollisionableImpl<MediumAsteroid> {

    public CollisionableMediumAsteroid(MediumAsteroid object, ObjectsContainer objectsContainer) {
        super(object, objectsContainer, MEDIUM_ASTEROID_WIDTH, MEDIUM_ASTEROID_HEIGHT);
    }

    @Override
    public void collisionedWith(CollisionableGameObject collisionable) {
        collisionable.collisionedWithAsteroid(object);
    }

    @Override
    public void collisionedWithBullet(Bullet bullet) {
        objectsContainer.removeGameObject(object);
    }

    @Override
    public void collisionedWithSpaceship(Spaceship spaceship) { objectsContainer.removeGameObject(object); }
}
