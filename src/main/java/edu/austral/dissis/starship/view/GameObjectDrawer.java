package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.model.*;
import edu.austral.dissis.starship.utils.Stats;
import processing.core.PGraphics;
import processing.core.PImage;

import static edu.austral.dissis.starship.utils.Utils.*;

public class GameObjectDrawer {

    private PImage spaceshipImage;
    private PImage bulletImage;
    private PImage bigAsteroidImage;
    private PImage mediumAsteroidImage;
    private PImage smallAsteroidImage;

    public GameObjectDrawer() {
    }

    public GameObjectDrawer setSpaceshipImage(PImage spaceshipImage){
        this.spaceshipImage = spaceshipImage;
        return this;
    }

    public GameObjectDrawer setBulletImage(PImage bulletImage){
        this.bulletImage = bulletImage;
        return this;
    }

    public GameObjectDrawer setBigAsteroidImage(PImage bigAsteroidImage){
        this.bigAsteroidImage = bigAsteroidImage;
        return this;
    }

    public GameObjectDrawer setMediumAsteroidImage(PImage mediumAsteroidImage){
        this.mediumAsteroidImage = mediumAsteroidImage;
        return this;
    }

    public GameObjectDrawer setSmallAsteroidImage(PImage smallAsteroidImage){
        this.smallAsteroidImage = smallAsteroidImage;
        return this;
    }

    public void draw(PGraphics graphics, Spaceship spaceship) {
        final Vector2 position = spaceship.getPosition();
        final float angle = calculateRotation(spaceship);

        graphics.pushMatrix();

        graphics.translate(position.getX(), position.getY());
        graphics.rotate(angle);

        graphics.image(spaceshipImage, SPACESHIP_WIDTH/-2f, SPACESHIP_HEIGHT/-2f);

        graphics.popMatrix();

    }

    public void draw(PGraphics graphics, BigAsteroid bigAsteroid) {
        final Vector2 position = bigAsteroid.getPosition();
        final float angle = calculateRotation(bigAsteroid);

        graphics.pushMatrix();

        graphics.translate(position.getX(), position.getY());
        graphics.rotate(angle);

        graphics.image(bigAsteroidImage, BIG_ASTEROID_WIDTH/-2f, BIG_ASTEROID_HEIGHT/-2f);

        graphics.popMatrix();

    }

    public void draw(PGraphics graphics, MediumAsteroid mediumAsteroid) {
        final Vector2 position = mediumAsteroid.getPosition();
        final float angle = calculateRotation(mediumAsteroid);

        graphics.pushMatrix();

        graphics.translate(position.getX(), position.getY());
        graphics.rotate(angle);

        graphics.image(mediumAsteroidImage, MEDIUM_ASTEROID_WIDTH/-2f,MEDIUM_ASTEROID_HEIGHT/-2f);

        graphics.popMatrix();

    }

    public void draw(PGraphics graphics, SmallAsteroid smallAsteroid) {
        final Vector2 position = smallAsteroid.getPosition();
        final float angle = calculateRotation(smallAsteroid);

        graphics.pushMatrix();

        graphics.translate(position.getX(), position.getY());
        graphics.rotate(angle);

        graphics.image(smallAsteroidImage, SMALL_ASTEROID_WIDTH/-2f, SMALL_ASTEROID_HEIGHT/-2f);

        graphics.popMatrix();

    }

    public void draw(PGraphics graphics, RegularBullet regularBullet) {
        final Vector2 position = regularBullet.getPosition();
        final float angle = calculateRotation(regularBullet);

        graphics.pushMatrix();

        graphics.translate(position.getX(), position.getY());
        graphics.rotate(angle);

        graphics.image(bulletImage, BULLET_WIDTH/-2f, BULLET_HEIGHT/-2f);


        graphics.popMatrix();

    }

    public void drawStats(PGraphics graphics, Stats stats) {
        stats.getSpaceshipLives().forEach((spaceshipId, lives) -> {
            graphics.fill(0,255,0);
            graphics.text("Player " + spaceshipId + ": lives: " + lives + ", points: " + stats.getSpaceshipPoints(spaceshipId), 200*spaceshipId, 10);
        });
    }
}
