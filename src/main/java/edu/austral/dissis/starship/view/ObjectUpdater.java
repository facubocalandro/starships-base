package edu.austral.dissis.starship.view;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.controller.*;
import edu.austral.dissis.starship.model.*;
import edu.austral.dissis.starship.utils.Pair;

import java.util.*;

import static edu.austral.dissis.starship.utils.Utils.*;

public class ObjectUpdater {
    private final ObjectsContainer objectsContainer;
    private final Map<KeyAdapter, InputHandler<GameObject>> adapterAndHandlers;
    private long timeSinceLastAsteroid;

    public ObjectUpdater(ObjectsContainer objectsContainer){
        this.objectsContainer = objectsContainer;

        adapterAndHandlers = new HashMap<>();

        for (int i = 0; i < objectsContainer.getSpaceships().size(); i++) {
            Spaceship spaceship = objectsContainer.getSpaceships().get(i);
            InputHandler<GameObject> compositeHandler = new CompositeHandler<>(new ShootHandler<>(spaceship), new MovementHandler<>(spaceship));
            adapterAndHandlers.put(new KeyAdapter(getKeys(i)), compositeHandler);
        }
        timeSinceLastAsteroid = 0;
    }

    public GameObject updateBullet(RegularBullet regularBullet){
        return regularBullet.moveForward(3);
    }

    public GameObject updateAsteroid(Asteroid asteroid){
        int randomMovement = new Random().nextInt(1000);
        if (randomMovement < 800){
            return asteroid.moveForward(ASTEROID_SPEED);
        } else if (randomMovement < 900){
            return asteroid.rotate(ROTATION);
        }
        return asteroid.rotate(-ROTATION);
    }

    public GameObject updateSpaceship(Spaceship spaceship) { return spaceship; }

    public void updateObjects(Set<Integer> keySet) {
        adapterAndHandlers.keySet().forEach(keyAdapter -> {
            keySet.forEach(key -> keyAdapter.adapt(key).ifPresent(customKey -> {
                Pair<Optional<GameObject>, InputHandler<GameObject>> pair = adapterAndHandlers.get(keyAdapter).handle(customKey);
                pair.first.ifPresent(objectsContainer::replaceGameObject);
                adapterAndHandlers.replace(keyAdapter, pair.second);
            }));
        });

        addAsteroid();
    }

    private void addAsteroid() {
        List<GameObject> newObjects = new ArrayList<>();
        objectsContainer.getObjects().forEach(gameObject -> newObjects.add(gameObject.update(this)));

        if(shouldAddAsteroid()) {
            Vector2 initialPosition;
            Vector2 initialDirection;

            int randomSide = new Random().nextInt(4);
            int randomXPosition = new Random().nextInt(WINDOW_WIDTH);
            int randomYPosition = new Random().nextInt(WINDOW_HEIGHT);
            switch (randomSide){
                case 0:
                    initialPosition = Vector2.vector(0, randomYPosition);
                    initialDirection = Vector2.vector(1,0);
                    break;
                case 1:
                    initialPosition = Vector2.vector(randomXPosition, 0);
                    initialDirection = Vector2.vector(0,1);
                    break;
                case 2:
                    initialPosition = Vector2.vector(WINDOW_WIDTH, randomYPosition);
                    initialDirection = Vector2.vector(-1,0);
                    break;
                default:
                    initialPosition = Vector2.vector(randomXPosition, WINDOW_HEIGHT);
                    initialDirection = Vector2.vector(0,-1);
                    break;
            }

            int size = new Random().nextInt(3);
            switch (size){
                case 0:
                    newObjects.add(new BigAsteroid(initialPosition, initialDirection, currentId++));
                    break;
                case 1:
                    newObjects.add(new MediumAsteroid(initialPosition, initialDirection, currentId++));
                    break;
                case 2:
                    newObjects.add(new SmallAsteroid(initialPosition, initialDirection, currentId++));
                    break;
            }
            timeSinceLastAsteroid = System.currentTimeMillis();
        }

        objectsContainer.setObjects(newObjects);
    }

    private boolean shouldAddAsteroid() {
        return System.currentTimeMillis() - timeSinceLastAsteroid > ASTEROID_INTERVAL;
    }
}
