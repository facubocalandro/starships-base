package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.view.CollisionableGameObject;
import edu.austral.dissis.starship.view.CollisionableSmallAsteroid;
import edu.austral.dissis.starship.view.GameObjectDrawer;
import processing.core.PGraphics;

public class SmallAsteroid extends Asteroid{
    public SmallAsteroid(Vector2 position, Vector2 direction, long id) {
        super(position, direction, id);
    }

    @Override
    public Asteroid rotate(float angle) {
        return new SmallAsteroid(position, direction.rotate(angle), id);
    }

    @Override
    public Asteroid moveForward(float speed) {
        return new SmallAsteroid(position.add(direction.multiply(speed)), direction, id);
    }

    @Override
    public Asteroid moveBackwards(float speed) {
        return new SmallAsteroid(position.subtract(direction.multiply(speed)), direction, id);
    }

    @Override
    public CollisionableGameObject asCollisionable(ObjectsContainer objectsContainer) {
        return new CollisionableSmallAsteroid(this, objectsContainer);
    }

    @Override
    public void draw(PGraphics graphics, GameObjectDrawer drawer) {
        drawer.draw(graphics, this);
    }
}
