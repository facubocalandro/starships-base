package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;

import java.util.Optional;

import static edu.austral.dissis.starship.utils.Utils.*;

public class RegularShootStrategy implements ShootStrategy{
    private long timeSinceLastShot;
    private long remainingBullets;

    public RegularShootStrategy(){
        timeSinceLastShot = 0;
        remainingBullets = INITIAL_BULLETS;
    }

    @Override
    public Optional<Bullet> shoot(Vector2 position, Vector2 direction, Shootable shooter) {
        if (canShoot()) {
            timeSinceLastShot = System.currentTimeMillis();
            remainingBullets--;
            return Optional.of(new RegularBullet(position, direction, currentId++, shooter.getId()));
        }
        return Optional.empty();
    }

    private boolean canShoot(){
        return System.currentTimeMillis() - timeSinceLastShot > SHOOT_INTERVAL && remainingBullets > 0;
    }
}
