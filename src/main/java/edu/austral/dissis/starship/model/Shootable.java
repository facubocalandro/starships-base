package edu.austral.dissis.starship.model;

import java.util.Optional;

public interface Shootable {
    Optional<Bullet> shoot();
    long getId();
}
