package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.controller.Updatable;
import edu.austral.dissis.starship.view.AsCollisionable;
import edu.austral.dissis.starship.view.Drawable;
import edu.austral.dissis.starship.view.Locatable;

public interface GameObject extends Locatable, Drawable, Updatable, AsCollisionable {
}
