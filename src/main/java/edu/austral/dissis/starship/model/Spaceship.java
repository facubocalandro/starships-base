package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.view.ObjectUpdater;
import edu.austral.dissis.starship.view.CollisionableGameObject;
import edu.austral.dissis.starship.view.CollisionableSpaceship;
import edu.austral.dissis.starship.view.GameObjectDrawer;
import processing.core.PGraphics;

import java.util.Optional;

public class Spaceship implements GameObject, Movable<Spaceship>, Shootable{
    private final Vector2 position;
    private final Vector2 direction;
    private final ShootStrategy shootStrategy;
    private final long id;

    public Spaceship(Vector2 position, Vector2 direction, ShootStrategy shootStrategy, long id) {
        this.position = position;
        this.direction = direction.asUnitary();
        this.shootStrategy = shootStrategy;
        this.id = id;
    }

    @Override
    public Spaceship rotate(float angle) {
        return new Spaceship(position, direction.rotate(angle), shootStrategy, id);
    }

    @Override
    public Spaceship moveForward(float speed) {
        return new Spaceship(position.add(direction.multiply(speed)), direction, shootStrategy, id);
    }

    @Override
    public Spaceship moveBackwards(float speed) {
        return new Spaceship(position.subtract(direction.multiply(speed)), direction, shootStrategy, id);
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Vector2 getDirection() {
        return direction;
    }

    @Override
    public void draw(PGraphics graphics, GameObjectDrawer drawer) {
        drawer.draw(graphics, this);
    }

    @Override
    public Optional<Bullet> shoot() { return shootStrategy.shoot(position, direction, this);}

    public Spaceship setShootStrategy(ShootStrategy shootStrategy){
        return new Spaceship(position, direction, shootStrategy, id);
    }

    @Override
    public GameObject update(ObjectUpdater objectUpdater) {
        return objectUpdater.updateSpaceship(this);
    }

    @Override
    public CollisionableGameObject asCollisionable(ObjectsContainer objectsContainer) {
        return new CollisionableSpaceship(this, objectsContainer);
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Spaceship)) return false;
        return ((Spaceship) obj).getId() == id;
    }

    @Override
    public boolean canMoveForward(Vector2 bounds, float speed) {
        Vector2 nextPosition = position.add(direction.multiply(speed));
        return nextPosition.getX() < bounds.getX() && nextPosition.getX() > 0 && nextPosition.getY() < bounds.getY() && nextPosition.getY() > 0;
    }

    @Override
    public boolean canMoveBackwards(Vector2 bounds, float speed) {
        Vector2 nextPosition = position.subtract(direction.multiply(speed));
        return nextPosition.getX() < bounds.getX() && nextPosition.getX() > 0 && nextPosition.getY() < bounds.getY() && nextPosition.getY() > 0;
    }
}
