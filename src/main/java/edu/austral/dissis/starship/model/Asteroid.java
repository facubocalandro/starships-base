package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.view.ObjectUpdater;

public abstract class Asteroid implements GameObject, Movable<Asteroid>{
    final Vector2 position;
    final Vector2 direction;
    final long id;

    public Asteroid(Vector2 position, Vector2 direction, long id){
        this.position = position;
        this.direction = direction;
        this.id = id;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Vector2 getDirection() {
        return direction;
    }

    @Override
    public GameObject update(ObjectUpdater objectUpdater) {
        return objectUpdater.updateAsteroid(this);
    }

    @Override
    public long getId() {return id;}

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Asteroid)) return false;
        return ((Asteroid)obj).getId() == id;
    }
}
