package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.utils.Stats;

import java.util.*;
import java.util.stream.Collectors;

import static edu.austral.dissis.starship.utils.Utils.currentId;

public class ObjectsContainer {
    private List<GameObject> objects;
    private List<Spaceship> spaceships;
    private final Stats stats;


    public ObjectsContainer() {
        objects = new ArrayList<>();
        spaceships = new ArrayList<>();
        Spaceship spaceship = new Spaceship(Vector2.vector(200,200), Vector2.vector(0,-1), new RegularShootStrategy(), currentId++);
        Spaceship spaceship1 = new Spaceship(Vector2.vector(400,400), Vector2.vector(0,-1), new RegularShootStrategy(), currentId++);
        spaceships.add(spaceship);
        spaceships.add(spaceship1);
        objects.add(spaceship);
        objects.add(spaceship1);
        stats = new Stats(spaceships);
    }

    public List<GameObject> getObjects() {
        return objects;
    }

    public List<Spaceship> getSpaceships() {
        return spaceships;
    }

    public void removeGameObject(GameObject gameObject){
        objects.remove(gameObject);
    }

    public void reduceLives(Long spaceshipId, Integer lives){
        if (stats.getSpaceshipLives(spaceshipId) <= 0){
            spaceships.stream().filter(spaceship -> spaceship.getId() == spaceshipId).findFirst().ifPresent(this::removeGameObject);
        } else {
            stats.reduceLives(spaceshipId, 1);
        }
    }

    public void increasePoints(Long spaceshipId, Integer points){
        stats.increasePoints(spaceshipId, points);
    }

    public void setObjects(List<GameObject> objects) {
        this.objects = objects;
    }

    public Stats getStats() {
        return stats;
    }

    public void replaceGameObject(GameObject gameObject) {
        if (objects.contains(gameObject)) {
            this.objects = this.objects.stream().map(object -> {
                if (object.equals(gameObject)) {
                    return gameObject;
                }
                return object;
            }).collect(Collectors.toList());
        } else {
            objects.add(gameObject);
        }

        if (gameObject instanceof Spaceship){
            if(spaceships.contains(gameObject)) {
                this.spaceships = this.spaceships.stream().map(spaceship -> {
                    if (spaceship.equals(gameObject)) {
                        return (Spaceship)gameObject;
                    }
                    return spaceship;
                }).collect(Collectors.toList());
            }else {
                spaceships.add((Spaceship) gameObject);
            }
        }
    }
}
