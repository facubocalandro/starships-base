package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;

public interface Movable<T> {
    T moveForward(float speed);
    T moveBackwards(float speed);
    T rotate(float degrees);
    default boolean canMoveForward(Vector2 bounds, float speed){ return true; }
    default boolean canMoveBackwards(Vector2 bounds, float speed){ return true; }
    long getId();
}
