package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;
import edu.austral.dissis.starship.view.ObjectUpdater;
import edu.austral.dissis.starship.view.CollisionableBullet;
import edu.austral.dissis.starship.view.CollisionableGameObject;
import edu.austral.dissis.starship.view.GameObjectDrawer;
import processing.core.PGraphics;


public class RegularBullet implements Bullet, Movable<Bullet>{
    private final Vector2 position;
    private final Vector2 direction;
    private final long id;
    private final long shooterId;

    public RegularBullet(Vector2 position, Vector2 direction, long id, long shooterId) {
        this.position = position;
        this.direction = direction;
        this.id = id;
        this.shooterId = shooterId;
    }

    @Override
    public void draw(PGraphics graphics, GameObjectDrawer drawer) {
        drawer.draw(graphics, this);
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Vector2 getDirection() {
        return direction;
    }

    @Override
    public GameObject update(ObjectUpdater objectUpdater) {
        return objectUpdater.updateBullet(this);
    }

    @Override
    public RegularBullet rotate(float angle) {
        return new RegularBullet(position, direction.rotate(angle), id, shooterId);
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public RegularBullet moveForward(float speed) {
        return new RegularBullet(position.add(direction.multiply(speed)), direction, id, shooterId);
    }

    @Override
    public RegularBullet moveBackwards(float speed) {
        return new RegularBullet(position.subtract(direction.multiply(speed)), direction, id, shooterId);
    }

    @Override
    public CollisionableGameObject asCollisionable(ObjectsContainer objectsContainer) {
        return new CollisionableBullet(this, objectsContainer);
    }

    @Override
    public long getShooterId() {
        return shooterId;
    }
}
