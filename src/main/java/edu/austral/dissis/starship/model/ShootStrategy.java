package edu.austral.dissis.starship.model;

import edu.austral.dissis.starship.base.vector.Vector2;

import java.util.Optional;

public interface ShootStrategy {
    Optional<Bullet> shoot(Vector2 position, Vector2 direction, Shootable shootable);
}
