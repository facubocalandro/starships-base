package edu.austral.dissis.starship.model;

public interface Bullet extends GameObject {
    long getShooterId();
}
