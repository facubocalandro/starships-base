package edu.austral.dissis.starship;

import edu.austral.dissis.starship.base.collision.CollisionEngine;
import edu.austral.dissis.starship.base.framework.GameFramework;
import edu.austral.dissis.starship.base.framework.ImageLoader;
import edu.austral.dissis.starship.base.framework.WindowSettings;
import edu.austral.dissis.starship.model.*;
import edu.austral.dissis.starship.view.GameObjectDrawer;
import edu.austral.dissis.starship.view.ObjectUpdater;
import processing.core.PGraphics;
import processing.event.KeyEvent;

import java.util.Set;
import java.util.stream.Collectors;

import static edu.austral.dissis.starship.utils.Utils.*;

public class CustomGameFramework implements GameFramework {
    private final ObjectsContainer objectsContainer;
    private final CollisionEngine collisionEngine;
    private GameObjectDrawer drawer;
    private final ObjectUpdater objectUpdater;

    public CustomGameFramework() {
        collisionEngine = new CollisionEngine();
        objectsContainer = new ObjectsContainer();
        objectUpdater = new ObjectUpdater(objectsContainer);
    }

    @Override
    public void setup(WindowSettings windowsSettings, ImageLoader imageLoader) {
        windowsSettings
            .setSize(1000, 1000);

        drawer = new GameObjectDrawer()
                .setSpaceshipImage(getSpaceshipImage(imageLoader))
                .setBigAsteroidImage(getBigAsteroidImage(imageLoader))
                .setMediumAsteroidImage(getMediumAsteroidImage(imageLoader))
                .setSmallAsteroidImage(getSmallAsteroidImage(imageLoader))
                .setBulletImage(getBulletImage(imageLoader));
    }

    @Override
    public void draw(PGraphics graphics, float timeSinceLastDraw, Set<Integer> keySet) {
        updateObjects(keySet);
        drawObjects(graphics);
        checkCollisions();
    }

    private void updateObjects(Set<Integer> keySet) {
        objectUpdater.updateObjects(keySet);
    }

    private void drawObjects(PGraphics graphics){
        objectsContainer.getObjects().forEach(gameObject -> gameObject.draw(graphics, drawer));
        drawer.drawStats(graphics, objectsContainer.getStats());
    }

    private void checkCollisions() {
        collisionEngine.checkCollisions(objectsContainer.getObjects().stream()
                .map(gameObject -> gameObject.asCollisionable(objectsContainer))
                .collect(Collectors.toList()));
    }

    @Override
    public void keyPressed(KeyEvent event) {
    }

    @Override
    public void keyReleased(KeyEvent event) {

    }
}
